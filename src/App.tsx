import { BrowserRouter, Routes, Route } from 'react-router-dom';

import NotFound from './page/notFound';
import Channels from './page/channels';
import Details from './page/details';
import Home from './page/main';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/channels/:channel' element={<Channels />} />
        <Route path='/details/:id' element={<Details />} />
        <Route path='/' element={<Home />} />
        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
