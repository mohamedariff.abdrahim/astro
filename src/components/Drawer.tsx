import { Button, Col, Divider, Drawer, Row, Typography } from "antd";
import { SortDescendingOutlined } from "@ant-design/icons";

interface FilterDrawerProps {
  onClose: any;
  visible: boolean;
  categories: String[] | undefined;
  languages: String[] | undefined;
  sortByName: any;
  sortByNumber: any;
  applyFilters: any;
}
const FilterDrawer = ({
  onClose,
  visible,
  categories,
  languages,
  sortByName,
  sortByNumber,
  applyFilters,
}: FilterDrawerProps) => {
  return (
    <Drawer
      title="Filters"
      placement="right"
      onClose={onClose}
      visible={visible}
    >
      <Row>
        <Typography.Text strong style={{ marginBottom: 10 }}>
          Sort
        </Typography.Text>
        <Row>
          <Button onClick={sortByName} icon={<SortDescendingOutlined />}>
            <Typography.Text style={{ fontSize: 12 }}>
              Sort by Channel Name
            </Typography.Text>
          </Button>

          <Button onClick={sortByNumber} icon={<SortDescendingOutlined />}>
            <Typography.Text style={{ fontSize: 12 }}>
              Sort byChannel Number
            </Typography.Text>
          </Button>
        </Row>
        <Divider />

        <Typography.Text strong style={{ marginBottom: 10 }}>
          Categories
        </Typography.Text>
        <Row gutter={[5, 5]}>
          {categories?.map((category: String, index: number) => {
            return (
              <Col key={index}>
                <Button onClick={() => applyFilters("category", category)}>
                  {category}
                </Button>
              </Col>
            );
          })}
        </Row>
        <Divider />

        <Typography.Text strong style={{ marginBottom: 10 }}>
          Languages
        </Typography.Text>
        <Row gutter={[5, 5]}>
          {languages?.map((language: String, index: number) => {
            return (
              <Col key={index}>
                <Button onClick={() => applyFilters("language", language)}>
                  {language}
                </Button>
              </Col>
            );
          })}
        </Row>
        <Divider />

        <Typography.Text strong style={{ marginBottom: 10 }}>
          Resolution
        </Typography.Text>
        <Row gutter={[5, 5]}>
          <Row>
            <Button onClick={() => applyFilters("isHd", true)}>HD</Button>
          </Row>
        </Row>
      </Row>
    </Drawer>
  );
};

export default FilterDrawer;
