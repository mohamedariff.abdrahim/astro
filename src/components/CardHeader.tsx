import { memo } from 'react';
import { Col, Row, Typography } from 'antd';

function CardHeader({ channel }: any) {
  return (
    <Row align='middle'>
      <img
        src={channel.imageUrl || channel.backupImage}
        alt={channel.title}
        width={48}
        height={28}
      />
      <Col style={{ marginLeft: 10 }}>
        <Row>
          <Typography.Text style={{ fontWeight: 400 }}>
            {`CH${channel.stbNumber}`}
          </Typography.Text>
        </Row>
        <Row>
          <Typography.Text strong>{channel.title}</Typography.Text>
        </Row>
      </Col>
    </Row>
  );
}

export default memo(CardHeader);
