import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { memo, useCallback, useState } from 'react';
import { Button, Card, Col, Row, Typography } from 'antd';
import { LikeFilled, LikeOutlined } from '@ant-design/icons';

import CardHeader from './CardHeader';

function ChannelCard({ channel }: any) {
  const navigate = useNavigate();

  const [isFav, setIsFav] = useState(localStorage.getItem(channel.id) || false);

  const handleOnClick = useCallback(() => {
    navigate(channel.detailUrl, { state: { id: channel.id } });
  }, [channel.detailUrl, channel.id, navigate]);

  const handleFavourite = (e: any) => {
    e.stopPropagation();
    setIsFav(true);
    localStorage.setItem(channel.id, 'true');
  };

  const handleUnfavourite = (e: any) => {
    e.stopPropagation();
    setIsFav(false);
    localStorage.removeItem(channel.id);
  };

  return (
    <Col>
      <Card
        bordered
        size='small'
        onClick={handleOnClick}
        style={{ width: 300, height: 170, cursor: 'pointer' }}
        title={<CardHeader channel={channel} />}
        extra={
          isFav ? (
            <Button onClick={handleUnfavourite}>
              <LikeFilled />
            </Button>
          ) : (
            <Button onClick={handleFavourite}>
              <LikeOutlined />
            </Button>
          )
        }
      >
        {channel.currentSchedule.slice(0, 3).map((show: any, index: number) => {
          return (
            <Row key={show.eventId}>
              <Col span={7}>
                <Typography.Text
                  strong={index === 0}
                  style={{ color: index === 0 ? undefined : 'grey' }}
                >
                  {index === 0
                    ? 'On Now'
                    : moment(show.datetime).format('hh:mm A')}
                </Typography.Text>
              </Col>
              <Col span={17}>
                <Typography.Text
                  ellipsis
                  strong={index === 0}
                  style={{ color: index === 0 ? undefined : 'grey' }}
                >
                  {show.title}
                </Typography.Text>
              </Col>
            </Row>
          );
        })}
      </Card>
    </Col>
  );
}

export default memo(ChannelCard);
