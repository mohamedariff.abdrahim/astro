export interface Schedule {
  datetime: String;
  datetimeInUtc: String;
  detailUrl: String;
  duration: String;
  episodeId: String;
  eventId: String;
  programmeId: String;
  siTrafficKey: String;
  title: String;
}

export interface ChannelState {
  backupImage: String;
  category: String;
  description: String;
  filters: String[];
  id: Number;
  imageUrl: String;
  isAstroGoExclusive: Boolean;
  isHd: Boolean;
  language: String;
  originalImage: String;
  schedule: Schedule[];
  stbNumber: String;
  title: String;
}

export interface LocationState {
  hash: String;
  key: String;
  pathname: String;
  search: String;
  state: {
    id: Number;
  };
}
