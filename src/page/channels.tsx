import { Breadcrumb, Col, Row, Skeleton, Tabs, Typography } from 'antd';
import { useLocation, useNavigate } from 'react-router-dom';
import { useCallback, useEffect, useState } from 'react';
import moment from 'moment';

import CardHeader from '../components/CardHeader';
import { ChannelState } from '../types';

function Channels() {
  const navigate = useNavigate();
  const { state, pathname } = useLocation();

  const [channel, setChannel] = useState<ChannelState | undefined>(undefined);

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<null | boolean | any>(false);

  useEffect(() => {
    const getChannelDetails = async () => {
      setLoading(true);
      try {
        const response = await fetch(
          //@ts-ignore
          `https://contenthub-api.eco.astro.com.my/channel/${state.id}.json`
        );
        const data = await response.json();
        setChannel(data?.response);
        setLoading(false);
        setError(null);
      } catch (error) {
        setError(error);
        setLoading(false);
        console.log('Error GetChannelDetails:', error);
      }
    };
    getChannelDetails();
    //@ts-ignore
  }, [state.id]);

  function tabsOnChange(key: string) {
    console.log(key);
  }

  const onClickDetails = useCallback(
    (show: any) => {
      navigate(show.detailUrl, { state: show });
    },
    [navigate]
  );

  if (loading || !channel) return <Skeleton />;

  return (
    <Row justify='center' style={{ padding: `20px 40px` }}>
      <Col>
        <Row style={{ marginBottom: 20 }}>
          <Breadcrumb>
            <Breadcrumb.Item onClick={() => navigate('/')}>
              <Typography.Text style={{ cursor: 'pointer', color: 'grey' }}>
                Content
              </Typography.Text>
            </Breadcrumb.Item>
            {pathname && (
              <Breadcrumb.Item>
                <Typography.Text>
                  {pathname.split('/')[pathname.split('/').length - 1]}
                </Typography.Text>
              </Breadcrumb.Item>
            )}
          </Breadcrumb>
        </Row>
        <Row style={{ marginBottom: 10 }}>
          <CardHeader channel={channel} />
        </Row>
        <Row style={{ marginBottom: 20 }}>
          <Typography.Text>{channel?.description}</Typography.Text>
        </Row>

        <Tabs defaultActiveKey='1' onChange={tabsOnChange}>
          {Object.entries(channel.schedule).map((schedule: any, i: number) => {
            const date = schedule[0];
            const data = schedule[1];
            return (
              <Tabs.TabPane
                tab={
                  i === 0 ? 'TODAY' : moment(date).format('ddd').toUpperCase()
                }
                key={date}
              >
                {data.map((show: any, index: number) => {
                  if (!moment(show.datetimeInUtc).isSameOrAfter()) return <></>;
                  return (
                    <Row>
                      <Col span={7}>
                        <Typography.Text
                          style={{ color: index === 0 ? undefined : 'grey' }}
                          strong={index === 0}
                        >
                          {index === 0
                            ? 'On Now'
                            : moment(show.datetimeInUtc).format('hh:mm A')}
                        </Typography.Text>
                      </Col>
                      <Col span={17}>
                        <Typography.Text
                          style={{
                            color: index === 0 ? undefined : 'grey',
                            cursor: 'pointer',
                          }}
                          strong={index === 0}
                          onClick={() => onClickDetails(show)}
                        >
                          {show.title}
                        </Typography.Text>
                      </Col>
                    </Row>
                  );
                })}
              </Tabs.TabPane>
            );
          })}
        </Tabs>
      </Col>
    </Row>
  );
}

export default Channels;
