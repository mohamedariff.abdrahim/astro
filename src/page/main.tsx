import { useCallback, useEffect, useState } from "react";
import { FilterOutlined } from "@ant-design/icons";
import { Button, Col, Input, Row, Space, Spin } from "antd";
import { sortBy } from "lodash";

import { ChannelState } from "../types";
import ChannelCard from "../components/Card";
import FilterDrawer from "../components/Drawer";

function Main() {
  const [categories, setCategories] = useState<String[] | undefined>(undefined);
  const [languages, setLanguages] = useState<String[] | undefined>(undefined);

  const [filteredChannels, setFilteredChannels] = useState([]);
  const [channelListing, setChannelListing] = useState([]);

  const [filter, setFilter] = useState<string>("");
  const [visible, setVisible] = useState(false);

  const showDrawer = () => setVisible(true);
  const onClose = () => setVisible(false);

  useEffect(() => {
    const getAllChannels = async () => {
      try {
        const response = await fetch(
          "https://contenthub-api.eco.astro.com.my/channel/all.json"
        );
        const data = await response.json();
        setChannelListing(data?.response);
      } catch (error) {
        console.log("Error GetAllChannels:", error);
      }
    };
    getAllChannels();
  }, []);

  useEffect(() => {
    const categories = channelListing.map(
      (schedule: ChannelState) => schedule.category
    );
    const removeCategoriesDuplicates = [...Array.from(new Set(categories))];
    setCategories(removeCategoriesDuplicates);

    const languages = channelListing.map((schedule: any) => schedule.language);
    const removeLanguageDuplicates = [...Array.from(new Set(languages))];
    setLanguages(removeLanguageDuplicates);
  }, [channelListing]);

  const onSearch = useCallback((e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    setFilter(value);
    const filter = channelListing.filter(
      (channel: any) =>
        channel.stbNumber.includes(value) ||
        channel.title.includes(value.toUpperCase())
    );
    setFilteredChannels(filter);
  }, []);

  const applyFilters = useCallback(
    (type: string, value: any) => {
      setFilter(value);
      const filter = channelListing.filter((channel: any) => {
        if (type === "isHd") {
          return channel.isHd === true;
        } else {
          return channel[type].includes(value);
        }
      });
      setFilteredChannels(filter);
    },
    [channelListing]
  );

  let list = filter.length ? filteredChannels : channelListing;

  const sortByName = useCallback(() => {
    const sorted = sortBy(list, ["title"]);
    filter.length ? setFilteredChannels(sorted) : setChannelListing(sorted);
  }, [filter.length, list]);

  const sortByNumber = useCallback(() => {
    const sorted = sortBy(list, ["stbNumber"]);
    filter.length ? setFilteredChannels(sorted) : setChannelListing(sorted);
  }, [filter.length, list]);

  return (
    <Row justify="center" gutter={[10, 10]} style={{ padding: `100px 0px` }}>
      <Input.Search
        onChange={onSearch}
        style={{ width: "85%" }}
        placeholder="Search Channels, TV Shows, Movies"
      />
      <Button icon={<FilterOutlined />} onClick={showDrawer}>
        Filter
      </Button>

      <Space />

      {!channelListing.length ? (
        <Col
          style={{
            flex: 1,
            height: "20vh",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Spin />
        </Col>
      ) : (
        <>
          <FilterDrawer
            onClose={onClose}
            visible={visible}
            languages={languages}
            categories={categories}
            sortByName={sortByName}
            applyFilters={applyFilters}
            sortByNumber={sortByNumber}
          />

          <Row justify="center" gutter={[10, 10]}>
            {list?.map((channel: any) => (
              <ChannelCard key={channel.id} channel={channel} />
            ))}
          </Row>
        </>
      )}
    </Row>
  );
}

export default Main;
